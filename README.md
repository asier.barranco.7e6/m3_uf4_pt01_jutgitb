# M3_UF4_PT02

- [Introducció](#introduccion)
- [Objetius UF4](#objetivos)
- [Funcions i classes](#funciones)
- [Conclusions](#conclusiones)

## Introducció <a name="introduccion"></a>
Aquesta és una aplicació que permet resoldre diferents problemes de programació.
La llista de problemes es defineix a la funció principal del programa. Actualment, els problemes disponibles són:

·Suma senzilla
·Factorial d'un nombre
·Comptar el nombre de vocals en una paraula
·Determinar el següent nombre primer
·Fer una operació mitjanament complicada.

Per a cada problema es proporciona una descripció, dos jocs de proves (un públic i un privat), una llista dels intents de l'usuari i la informació de si ha estat resolt.

## Objetius UF4 <a name="objetivos"></a>
L'objectiu sempre és aprende i millorar, però a més, en la UF4, el que es busca concretament és aprendre i familiaritzar-se amb l'ús de classes.

## Funcions i classes<a name="funciones"></a>


### Classe Problema:
Cada problema es defineix com a una instància de la classe Problema, que té els següents atributs:

enunciat: una descripció del problema.
jocDeProvesPublic: un parell que conté les dades d'entrada i sortida esperada del joc de proves públic.
jocDeProvesPrivat: un parell que conté les dades d'entrada i sortida esperada del joc de proves privat.
resolt: un valor booleà que indica si el problema ha estat resolt o no. El valor per defecte és false.
intents: un enter que indica el nombre d'intents que l'usuari ha necessitat per resoldre el problema. El valor per defecte és 0.
intentsUsuari: una llista de cadenes que conté els intents de l'usuari per resoldre el problema.

### Funcions dins la classe
A més, la classe Problema té els següents mètodes:

    -   mostrarJocDeProvesPublic(): mostra per pantalla el joc de proves públic del problema.
    -   joc(): mostra per pantalla l'enunciat i les dades d'entrada del joc de proves privat del problema.
    -   afegirIntent(resposta: String): afegeix un intent de l'usuari a la llista intentsUsuari i incrementa el comptador intents.
    -   comprovarResposta(resposta: String): Boolean: compara la resposta de l'usuari amb la sortida esperada del joc de proves privat i retorna true si són iguals, false      altrament.

### Funció principal
La funció principal del programa crea una llista de problemes i, per a cada problema, mostra l'enunciat i el joc de proves públic. Després, pregunta a l'usuari si vol resoldre el problema. Si la resposta és afirmativa, el programa mostra l'enunciat i les dades d'entrada del joc de proves privat i demana a l'usuari que introdueixi la seva solució. Si la solució és correcta, el programa felicita l'usuari i incrementa el comptador de problemes resolts. Si la solució és incorrecta, el programa informa l'usuari i li demana que torni a intentar-ho.

## Conclusions<a name="conclusiones"></a>
La meva opinió personal respecte aquest projecte és que és força entretingut i amè. Tot i que els meus problemes no són gaire complicats, pensar-los ha estat interessant. A més, allunyar-se una mica de la mateixa temàtica Wordle que portava un temps molt intens va bé.
Respecte a treballar amb classes, trobo molt útil i net fer servir-les.

##### Asier Barranco
