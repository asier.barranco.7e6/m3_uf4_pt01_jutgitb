/**
 * Classe que representa un problema amb l'enunciat, els jocs de proves públic i privat,
 * la informació de si ha estat resolt, el nombre d'intents que ha requerit la resolució
 * i la llista dels intents de l'usuari.
 *
 */
var problemesResolts = 0 //Contador per saber quants problemes ha resolt (Opció voluntaria i insignificant)
class Problema
    (val enunciat: String,
     val jocDeProvesPublic: Pair<String, String>,
     val jocDeProvesPrivat: Pair<String, String>,
     var resolt: Boolean = false,
     var intents: Int = 0,
     val intentsUsuari: MutableList<String> = mutableListOf()) {

    /**
     * Mostra el joc de proves públic d'un problema semblant per pantalla.
     */
    fun mostrarJocDeProvesPublic() {
        println("Joc de proves:")
        println("Entrada: ${jocDeProvesPublic.first}")
        println("Sortida esperada: ${jocDeProvesPublic.second}")
    }

    /**
     * Mostra l'enunciat del problema
     */
    fun joc() {
        println("Exercici a resoldre:")
        println(enunciat)
        println("Entrada: ${jocDeProvesPrivat.first}")
    }

    /**
     * Afegeix un intent de l'usuari a la llista d'intents i incrementa el comptador d'intents.
     *
     * @param resposta la resposta introduïda per l'usuari
     */
    fun afegirIntent(resposta: String) {
        intents++
        //Afegir l'intent de l'usuari a una llista
        intentsUsuari.add(resposta)
    }

    /**
     * Comprova si la resposta introduïda per l'usuari és correcta.
     *
     * @param resposta la resposta introduïda per l'usuari
     * @return true si la resposta és correcta, false altrament
     */
    fun comprovarResposta(resposta: String): Boolean {
        afegirIntent(resposta)
        //Si la resposta que ha introduit l'usuari correspon a l'esperada (la segona del joc de proves privat), retorna true
        return resposta == jocDeProvesPrivat.second
    }
}

/**
 * Funció principal del programa que gestiona una llista de problemes.
 */
fun main() {

    //Creem els problemes a partir de la classe
    val llistaProblemes = mutableListOf(
        Problema("Suma senzilla", Pair("1+1", "2"), Pair("33+77","110")),
        Problema("Factorial d'un nombre", Pair("4", "24"), Pair("5", "120")),
        Problema("Comptar el nombre de vocals en una paraula", Pair("hello", "2"), Pair("asier", "3")),
        Problema("Determinar el següent nombre primer", Pair("17", "19"), Pair("97", "101")),
        Problema("Fer una operació mitjanament complicada (No pots utilitzar calculadora)", Pair("21-6*(2+1)", "3"), Pair("(7^2-6*3)+1-100", "-68"))
    )

    //Posició del problema en que ens trobem
    var indexProblemaActual = 0

    //Mentre quedin problemes disponibles...
    while (indexProblemaActual < llistaProblemes.size) {

        //Agafem la posició del problema en que ens trobem de la llista amb tots els problemes
        val problemaActual = llistaProblemes[indexProblemaActual]

        //Mostrem l'enunciat i el joc de proves del problema
        println(problemaActual.enunciat)
        problemaActual.mostrarJocDeProvesPublic()

        //Preguntem si vol resoldre el problema
        print("Vols resoldre el problema? (S/N) ")
        when (readLine().toString()) {
            //Si el vol resoldre:
            "S" -> {

                //El problema haurà acabat o bé quan l'encerti, o es rendeixi.
                var finish = false
                while (!finish) { //Mentre no acabi el problema...

                    //Mostrem el joc del problema
                    problemaActual.joc()

                    //Demanem la resposta
                    print("Introdueix la resposta: ")
                    val respostaUsuari = readln()

                    //Passem la resposta de l'usuari per compararla amb l'esperada
                    if (problemaActual.comprovarResposta(respostaUsuari)) { //Si és correcta

                        //El felicitem
                        println("Resposta correcta!. I en només ${problemaActual.intents} intents!")
                        //Sumem als problemes resolts
                        problemesResolts++
                        problemaActual.resolt = true
                        //El bucle d'aquest problema finalitza
                        finish = true

                    } else { //Si la resposta no es correspon

                        println("Resposta incorrecta.")
                        //Informem del nombre d'intents que porta en aquest problema i demanem i es rendeix
                        print("Portes ${problemaActual.intents} intents. Vols passar al següent? (S/N) ")
                        if (readln() == "S") { //Si decideix rendirse

                            //Mostrem la solució per informar
                            println("La solució era ${problemaActual.jocDeProvesPrivat.second}.")
                            //El bucle d'aquest problema finalitza
                            finish = true

                        } else //Si no es rendeix
                            println("Això continua")
                    }
                }
            }
            "N" -> { //Si no vol resoldre aquest problema

                //Mostrem la solució per informar
                println("La solució era ${problemaActual.jocDeProvesPrivat.second}")
                println("Següent problema.")

            }
            else -> { //Si no introdueix que si o que no, error i passem al següent
                println("Resposta no vàlida. Passem al següent problema.")
            }
        }
        indexProblemaActual++ //Sumar una posició al problema actual
        }


    //Informar dels problemes resolts
    println("Has completat $problemesResolts problemes!")
}